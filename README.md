# README #

## Introduction:  

This is one of the many summer projects I will be developing using Ruby on Rails. Primary aim of these projects is to focus on learning Ruby on Rails (and not worry about styling the user interface) and apply my knowledge to implement complex features in my web application.

Some of the features implemented are:
* Creating articles and signing up users.
* Assigning admin functionality to users.
* Restricting actions by users based on their role.
* Associating one-to-many relationships b/w users and articles.
* Associating many-to-many relationships b/w articles and artiles categories.
* Unit, functional and integration tests written using Ruby test helpers.


## Technologies: ##

* HTML/ERB Templating
* CSS/Bootstrap
* Ruby on Rails
* Cloud 9 IDE

## Purpose: ##

* Become proficient in Ruby on Rails.
* Be well equipped for my Fall '16 internship with Shopify.

## Procedure: ##

1. Install [Ruby on Rails.](http://rubyonrails.org/)
2. Browse to the root folder and run the following command in the terminal: rails server -p 0.0.0.0 or rails server


## Timestamp: ##

**July, 2016